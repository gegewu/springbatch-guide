package com.example.Chapter06.job;

import com.example.Chapter06.domain.Transaction;
import org.springframework.batch.item.ItemProcessor;

public class ImportTransactionFileStepProcess implements ItemProcessor<Transaction ,Transaction> {
    @Override
    public Transaction process(Transaction item) throws Exception {
        if(item == null){
            return null;
        }else{
            return item;
        }

    }
}
