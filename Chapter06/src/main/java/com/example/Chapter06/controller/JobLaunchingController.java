package com.example.Chapter06.controller;

import com.example.Chapter06.domain.BatchJob;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@RestController
public class JobLaunchingController {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JobExplorer jobExplorer;

    @PostMapping(path = "/run")
    public ExitStatus runJob(@RequestBody JobLaunchingController.JobLaunchRequest request) throws Exception {
        Job job = this.context.getBean(request.getName(), BatchJob.class).getJob();

        JobParameters jobParameters =
                new JobParametersBuilder(request.getJobParameters(),
                        this.jobExplorer)
                        .getNextJobParameters(job)
                        .toJobParameters();

        return this.jobLauncher.run(job, jobParameters).getExitStatus();
//		Job job = this.context.getBean(request.getName(), Job.class);
//		return this.jobLauncher.run(job, request.getJobParameters()).getExitStatus();
    }

    public static class JobLaunchRequest {
        private String name;

        private Properties jobParameters;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Properties getJobParamsProperties() {
            return jobParameters;
        }

        public void setJobParamsProperties(Properties jobParameters) {
            this.jobParameters = jobParameters;
        }

        public JobParameters getJobParameters() {
            Properties properties = new Properties();
            properties.putAll(this.jobParameters);
            return new JobParametersBuilder(properties)
                    .toJobParameters();
        }
    }
}
