package com.example.Chapter06.controller;

import com.example.Chapter06.domain.BatchJob;
import com.example.Chapter06.util.CacheUtil;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@RestController
public class JobBeanController {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JobExplorer jobExplorer;

    @PostMapping(path = "/jobname/run")
    public ExitStatus runJob(@RequestBody JobBeanController.JobLaunchRequest request) throws Exception {

        JobParameters jobParameters = request.getJobParameters();
        Job job = null;
        String jobName = request.getName();
        if(jobName.equals("transactionJob")){
            // 作业作为一个组件bean以加载到容器中，直接获取
            job = this.context.getBean(request.getName(), Job.class);
        }else{
            // 组件方法获取job
            job = this.context.getBean(request.getName(), BatchJob.class).getJob();
        }


        /*JobParameters jobParameters =
                new JobParametersBuilder(request.getJobParameters(),
                        this.jobExplorer)
                        .getNextJobParameters(job)
                        .toJobParameters();

        return this.jobLauncher.run(job, jobParameters).getExitStatus();*/

		return this.jobLauncher.run(job, jobParameters).getExitStatus();
    }

    public static class JobLaunchRequest {
        private String name;

        private Properties jobParameters;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Properties getJobParamsProperties() {
            return jobParameters;
        }

        public void setJobParamsProperties(Properties jobParameters) {
            this.jobParameters = jobParameters;
        }

        public JobParameters getJobParameters() {
            //Properties properties = new Properties();
            //properties.putAll(this.jobParameters);

            Map<String, JobParameter> parameterMap = new HashMap<>();

            Set<Object> keys = this.jobParameters.keySet();
            for(Object param : keys){
                String strKey = (String)param;
                if(strKey.equals("job")){
                    parameterMap.put((String)param, new JobParameter((String) this.jobParameters.get(param), true)); //识别性参数（每次输入的这个值不同，就可以执行）
                }else{
                    parameterMap.put((String)param, new JobParameter((String) this.jobParameters.get(param), false)); //非识别性参数
                    CacheUtil.getInstance().addCacheData((String)param,this.jobParameters.get(param));// 参数加到缓存器中
                }

            }
            JobParameters jobParameters = new JobParameters(parameterMap);

            //return new JobParametersBuilder(properties).toJobParameters();
            return jobParameters;
        }
    }
}
