package com.example.Chapter06.domain;

import org.springframework.batch.core.Job;

import java.net.MalformedURLException;

public interface BatchJob {

    Job getJob() throws MalformedURLException;
}
