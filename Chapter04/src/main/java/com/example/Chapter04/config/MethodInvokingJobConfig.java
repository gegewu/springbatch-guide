package com.example.Chapter04.config;

import com.example.Chapter04.service.CustomService;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.tasklet.MethodInvokingTaskletAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class MethodInvokingJobConfig {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public CustomService service() {
        return new CustomService();
    }

    /**
     * job的参数自动注入，下面两个bean一起作用
     * @StepScope 、.tasklet(methodInvokingTasklet(null)) 、@Value("#{jobParameters['message']}") String message 一起作用的，原理暂时不太明白
     * @return
     */
    @Bean(name = "methodInvokingStep")
    public Step methodInvokingStep() {
        return this.stepBuilderFactory.get("methodInvokingStep")
                .tasklet(methodInvokingTasklet(null))
                .build();
    }
    @StepScope //spring延迟绑定功能，将作业参数注入到组件当中
    @Bean
    public MethodInvokingTaskletAdapter methodInvokingTasklet(
            @Value("#{jobParameters['message']}") String message) {

        MethodInvokingTaskletAdapter methodInvokingTaskletAdapter = new MethodInvokingTaskletAdapter();
        methodInvokingTaskletAdapter.setTargetObject(service());
        methodInvokingTaskletAdapter.setTargetMethod("serviceMethod"); //作业执行逻辑，执行service中的serviceMethod方法
        methodInvokingTaskletAdapter.setArguments(new String[] {message});

        return methodInvokingTaskletAdapter;
    }

}
