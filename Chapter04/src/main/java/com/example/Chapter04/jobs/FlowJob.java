/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.Chapter04.jobs;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

/**
 * 外部化流
 */
@Component
public class FlowJob extends AbstractBatchJob {

	
	public Tasklet loadStockFile() {
		return (contribution, chunkContext) -> {
			System.out.println("The stock file has been loaded");
			return RepeatStatus.FINISHED;
		};
	}

	
	public Tasklet loadCustomerFile() {
		return (contribution, chunkContext) -> {
			System.out.println("The customer file has been loaded");
			return RepeatStatus.FINISHED;
		};
	}

	
	public Tasklet updateStart() {
		return (contribution, chunkContext) -> {
			System.out.println("The start has been updated");
			return RepeatStatus.FINISHED;
		};
	}

	
	public Tasklet runBatchTasklet() {
		return (contribution, chunkContext) -> {
			System.out.println("The batch has been run");
			return RepeatStatus.FINISHED;
		};
	}

	/**
	 * Flow : 步骤流，其中封装了两个step
	 * @return
	 */
	public Flow preProcessingFlow() {
		return new FlowBuilder<Flow>("preProcessingFlow").start(loadFileStep())
				.next(loadCustomerStep())
				.next(updateStartStep())
				.build();
	}

	
	public Job getJob() {
		return this.jobBuilderFactory.get("flowJob")
				.start(intializeBatch())
				.next(runBatch())
				.build();
	}


	public Step intializeBatch() {
		return this.stepBuilderFactory.get("initalizeBatch")
				.flow(preProcessingFlow())// 步骤流封装到step中
				.build();
	}

	
	public Step loadFileStep() {
		return this.stepBuilderFactory.get("loadFileStep")
				.tasklet(loadStockFile())
				.build();
	}

	
	public Step loadCustomerStep() {
		return this.stepBuilderFactory.get("loadCustomerStep")
				.tasklet(loadCustomerFile())
				.build();
	}

	
	public Step updateStartStep() {
		return this.stepBuilderFactory.get("updateStartStep")
				.tasklet(updateStart())
				.build();
	}

	
	public Step runBatch() {
		return this.stepBuilderFactory.get("runBatch")
				.tasklet(runBatchTasklet())
				.build();
	}
    
}
