/**
 * Copyright 2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.Chapter04.jobs;

import java.util.Arrays;
import java.util.Map;

import com.example.Chapter04.batch.DailyJobTimestamper;
import com.example.Chapter04.batch.JobLoggerListener;
import com.example.Chapter04.batch.ParameterValidator;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.CompositeJobParametersValidator;
import org.springframework.batch.core.job.DefaultJobParametersValidator;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobListenerFactoryBean;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldJob extends AbstractBatchJob {

	@Autowired
    @Qualifier("helloWorldJobStep1")
	private Step helloWorldJobStep1;

    /**
     *
     * @return job 参数校验器
     */
    public CompositeJobParametersValidator validator() {
        //CompositeJobParametersValidator 混合参数校验器，可将多个参数校验器封装进去
        CompositeJobParametersValidator validator = new CompositeJobParametersValidator();

        //DefaultJobParametersValidator 默认参数校验器，校验参数是否存在
        DefaultJobParametersValidator defaultJobParametersValidator = new DefaultJobParametersValidator(
                        new String[]{"fileName","jobName"},//job必须参数
                        new String[]{"name","currentBatch","run.id","currentDate"});//job可选参数,job参数不能操作这个范围
        defaultJobParametersValidator.afterPropertiesSet();

        //ParameterValidator 自定义参数校验器
        validator.setValidators(Arrays.asList(new ParameterValidator(), defaultJobParametersValidator));

        return validator;
    }

    /**
     * 测试方法：JobRunTest#helloWorldJobTest
     * @return
     */
/*
    public Job getJob() {
        return this.jobBuilderFactory.get("helloWorldJob")
                .incrementer(new RunIdIncrementer())
                .start(HelloWorldJobStep())
                .build();
    }
*/

    public Job getJob() {

        return this.jobBuilderFactory.get("helloWorldJob")
                .start(helloWorldJobStep1)
                .validator(validator()) // 添加参数校验器
                /**
                 * incrementer 方法，给job封装一个“增量器”，作用就是每次执行job时，配合封装参数会自动变，标识JobInstance
                 * DailyJobTimestamper和RunIdIncrementer都实现JobParametersIncrementer接口，即都有一个getNext方法，在使用jobLauncher.run(...)
                 * +启动job时，最主要的就是封装 jobParameters（使用JobParametersBuilder#getNextJobParameters 方法，见JobController.java中代码），
                 * +这里就实现了自变化识别性参数作用与job执行。
                 */
//                .incrementer(new DailyJobTimestamper()) //JobParameters中增加一个时间戳参数 currentDate
                .incrementer(new RunIdIncrementer()) //JobParameters中增加一个自增id的参数 run.id

//				.listener(new JobLoggerListener()) //监听类：实现接口JobExecutionListener
                .listener(JobListenerFactoryBean.getListener(new JobLoggerListener())) //监听类：使用注解@BeforJob、@AfterJob，作业监听器封装需要注入的方式（使用JobListenerFactoryBean来完成）
                .build();
    }


    public Step HelloWorldJobStep() {
        return this.stepBuilderFactory.get("helloWorldJobStep")
                .tasklet(helloWorldTasklet())
                .build();
    }

    public Tasklet helloWorldTasklet() {

        return (contribution, chunkContext) -> {
            String name = (String) chunkContext.getStepContext()
                    .getJobParameters()
                    .get("name");// 获取作业参数

            System.out.println(String.format("Hello, %s!", name));
            return RepeatStatus.FINISHED;
        };
    }


}
